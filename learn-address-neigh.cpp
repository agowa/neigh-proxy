#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <cstring>
#include <arpa/inet.h>

using std::string;

bool is_ipv6_address(const string& str);

int main(int argc, char *argv[])
{
  string straction = string(argv[1]);
  string straddr = string(argv[2]);

  string stradd = "add";
  string strupdate = "update";
  string strdelete = "delete";

  if(is_ipv6_address(straddr))
  {
    if(strstr(straction.c_str(),stradd.c_str()))
    {
      system (("ip neigh replace proxy \"" + straddr + "\" dev eth0").c_str());
    } else if(strstr(straction.c_str(),strupdate.c_str())) {
      system (("ip neigh replace proxy \"" + straddr + "\" dev eth0").c_str());
    } else if(strstr(straction.c_str(),strdelete.c_str())) {
      system (("ip neigh delete proxy \"" + straddr + "\" dev eth0").c_str());
    } else {};
  };
  return 0;
}

bool is_ipv6_address(const string& str)
{
  struct sockaddr_in6 sa;
  return inet_pton(AF_INET6, str.c_str(), &(sa.sin6_addr))!=0;
};
